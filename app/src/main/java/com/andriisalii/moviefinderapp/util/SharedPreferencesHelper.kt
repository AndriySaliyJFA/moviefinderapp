package com.andriisalii.moviefinderapp.util

import android.content.Context
import android.content.SharedPreferences
import com.andriisalii.moviefinderapp.R

class SharedPreferencesHelper(
    context: Context
) {
    companion object {
        const val DEF_VALUE = "default"
    }

    private var sharedPreferences: SharedPreferences = context.getSharedPreferences(
        context.getString(R.string.shared_preferences), 0
    )

    fun writeData(key: String, data: String) {
        sharedPreferences.edit().putString(key, data).apply()
    }

    fun readData(key: String): String? {
        return sharedPreferences.getString(key, DEF_VALUE)
    }
}