package com.andriisalii.moviefinderapp.util

import com.andriisalii.moviefinderapp.App

class ResourceHelper{
    companion object{
        fun getString(stringId:Int):String{
            return App.context.resources.getString(stringId)
        }
    }
}