package com.andriisalii.moviefinderapp.dependencyInjections

import com.andriisalii.moviefinderapp.registration.loginScreen.LoginViewModel
import com.andriisalii.moviefinderapp.registration.repository.FirebaseManager
import com.andriisalii.moviefinderapp.registration.repository.UserDataRepository
import com.andriisalii.moviefinderapp.registration.signUpScreen.SignUpViewModel
import com.andriisalii.moviefinderapp.util.SharedPreferencesHelper
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val repositoryModule = module {
    single { FirebaseManager() }
    single { UserDataRepository(get()) }
    single { SharedPreferencesHelper(get()) }
}
val viewModelModule = module {
    viewModel { SignUpViewModel(get()) }
    viewModel { LoginViewModel(get()) }
}
