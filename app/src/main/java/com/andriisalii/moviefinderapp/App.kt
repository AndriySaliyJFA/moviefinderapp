package com.andriisalii.moviefinderapp

import android.app.Application
import android.content.Context
import com.andriisalii.moviefinderapp.dependencyInjections.repositoryModule
import com.andriisalii.moviefinderapp.dependencyInjections.viewModelModule
import com.google.firebase.auth.FirebaseAuth
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Main reason for this class is that it contains context to receive strings by ID
 * it helps to avoid writing separate BindAdapter for each String-bonded field
 *
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        context = applicationContext
        startKoin{
            androidContext(context)
            modules(listOf(repositoryModule, viewModelModule))
        }
    }

    companion object {
        private lateinit var instance: App
        lateinit var context: Context
            private set
    }
}