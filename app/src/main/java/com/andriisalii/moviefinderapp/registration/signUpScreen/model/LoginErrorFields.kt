package com.andriisalii.moviefinderapp.registration.signUpScreen.model

data class LoginErrorFields(
    var emailError: String?, var passwordError: String?
) {
    constructor() : this(null, null)
}
