package com.andriisalii.moviefinderapp.registration

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.andriisalii.moviefinderapp.R
import com.andriisalii.moviefinderapp.registration.loginScreen.LoginFragment

private const val UNIQUE_TAG = "uniqueTag"

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val fragment: Fragment? = supportFragmentManager.findFragmentByTag(UNIQUE_TAG)

        if (fragment == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.login_main_container,
                    LoginFragment(), UNIQUE_TAG)
                .commit()
        } else {
            supportFragmentManager.beginTransaction()
                .replace(R.id.login_main_container, fragment, UNIQUE_TAG)
                .commit()
        }
    }
}
