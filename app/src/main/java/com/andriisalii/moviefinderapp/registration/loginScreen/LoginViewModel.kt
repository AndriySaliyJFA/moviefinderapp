package com.andriisalii.moviefinderapp.registration.loginScreen

import androidx.lifecycle.ViewModel
import com.andriisalii.moviefinderapp.util.SingleLiveEvent
import com.andriisalii.moviefinderapp.registration.repository.UserDataRepository


class LoginViewModel(
    private val userDataRepository: UserDataRepository
) : ViewModel() {

    companion object {
        const val TAG = "LoginViewModel"
    }

    var launchState = SingleLiveEvent<LoginFragment.LaunchStates>()


    fun checkLaunchState() {
        if (userDataRepository.isUserLoggined()) {
            if (userDataRepository.isUserGuest()) {
                launchState.value = LoginFragment.LaunchStates.LAUNCH_AS_GUEST
            } else {
                launchState.value = LoginFragment.LaunchStates.LAUNCH_FOR_LOGINED
            }
        } else {
            launchState.value = LoginFragment.LaunchStates.LAUNCH_DEFAULT
        }
    }
}