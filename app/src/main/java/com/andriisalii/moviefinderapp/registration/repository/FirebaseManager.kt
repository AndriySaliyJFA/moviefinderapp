package com.andriisalii.moviefinderapp.registration.repository

import android.service.autofill.UserData
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth

class FirebaseManager : IUserData {
    private val firebaseAuth = FirebaseAuth.getInstance()

    override fun isUserGuest(): Boolean {
        return firebaseAuth.currentUser!!.isAnonymous
    }

    override fun isUserLoggined(): Boolean {
        return firebaseAuth.currentUser != null
    }

    override fun signInWithEmailAndPassword(email: String, password: String): Task<AuthResult> {
        return firebaseAuth.signInWithEmailAndPassword(email, password)
    }

    override fun createUserWithEmailAndPassword(email: String, password: String): Task<AuthResult> {
        return firebaseAuth.createUserWithEmailAndPassword(email, password)
    }
}