package com.andriisalii.moviefinderapp.registration.signUpScreen.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.andriisalii.moviefinderapp.R
import com.andriisalii.moviefinderapp.util.ResourceHelper
import com.andriisalii.moviefinderapp.util.ValidationUtils.Companion.isEmailValid
import com.andriisalii.moviefinderapp.util.ValidationUtils.Companion.isPasswordValid

class LoginForm : BaseObservable() {

    val loginFields: LoginFields = LoginFields()
    val loginErrorFields: LoginErrorFields = LoginErrorFields()

    @Bindable
    fun isLoginButtonEnabled(): Boolean {
        notifyPropertyChanged(BR.loginButtonEnabled)
        return isEmailValid(loginFields.email) &&
                isPasswordValid(loginFields.password)
    }

    fun checkEmail(isEmailNotEmpty: Boolean) {
        if (isEmailNotEmpty && isEmailValid(loginFields.email)) {
            loginErrorFields.emailError = null
            notifyChange()
        } else {
            loginErrorFields.emailError = ResourceHelper.getString(R.string.err_email)
            notifyChange()
        }
    }

    fun checkPassword(isPasswordNotEmpty: Boolean) {
        if (isPasswordNotEmpty && isPasswordValid(loginFields.password)) {
            loginErrorFields.passwordError = null
            notifyChange()
        } else {
            loginErrorFields.passwordError = ResourceHelper.getString(R.string.err_password)
            notifyChange()
        }
    }

    fun clearEmailError() {
        loginErrorFields.emailError = null
        notifyChange()
    }

    fun clearPasswordError() {
        loginErrorFields.passwordError = null
        notifyChange()
    }
}