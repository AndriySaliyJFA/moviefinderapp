package com.andriisalii.moviefinderapp.registration.signUpScreen

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.andriisalii.moviefinderapp.MainActivity
import com.andriisalii.moviefinderapp.R
import com.andriisalii.moviefinderapp.databinding.FSignUpBinding
import com.andriisalii.moviefinderapp.registration.loginScreen.LoginFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.f_sign_up.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SignUpFragment : Fragment() {

    private val mLoginViewModel: SignUpViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val mBinding: FSignUpBinding =
            DataBindingUtil.inflate(inflater, R.layout.f_sign_up, container, false)
        mBinding.viewModel = mLoginViewModel
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val launchState = LoginFragment.LaunchStates.values()[
                arguments!!.getInt(
                    LoginFragment.KEY_STATE,
                    LoginFragment.LaunchStates.LAUNCH_FOR_REGISTRATION.ordinal
                )]
        mLoginViewModel.launchState = launchState

        prepareUI(launchState)
        setupBindings()
    }

    private fun prepareUI(launchState: LoginFragment.LaunchStates) {
        when (launchState) {
            LoginFragment.LaunchStates.LAUNCH_FOR_LOGIN -> {
                btnCreateAcc.setText(R.string.login_email)
            }
            LoginFragment.LaunchStates.LAUNCH_FOR_REGISTRATION -> {
                btnCreateAcc.setText(R.string.action_sign_up)
            }
            else -> showError(R.string.err_login)
        }

    }

    private fun showError(stringResId: Int? = null, stringValue: String = "") {
        Snackbar.make(
            scroll_view_sign_up, if (stringResId == null) {
                stringValue
            } else {
                getString(stringResId)
            }, Snackbar.LENGTH_SHORT
        )
            .show()
    }

    private fun setupBindings() {
        mLoginViewModel.isSignInSuccessful.observe(viewLifecycleOwner, Observer { isSuccessful ->
            if (isSuccessful) {
                startMainActivity()
            }
        }
        )

        mLoginViewModel.errorMessage.observe(viewLifecycleOwner, Observer { errorMsg ->
            showError(stringValue = errorMsg)
        })
    }

    private fun startMainActivity() {
        val bundle = Bundle()
        bundle.putInt(
            LoginFragment.KEY_STATE,
            LoginFragment.LaunchStates.LAUNCH_FOR_LOGINED.ordinal
        )
        startActivity(Intent(context, MainActivity::class.java).putExtras(bundle))
        activity!!.finish()
    }
}