package com.andriisalii.moviefinderapp.registration.repository

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult

interface IUserData {
    fun isUserGuest(): Boolean

    fun isUserLoggined(): Boolean

    fun signInWithEmailAndPassword(email: String, password: String): Task<AuthResult>

    fun createUserWithEmailAndPassword(email: String, password: String): Task<AuthResult>

}