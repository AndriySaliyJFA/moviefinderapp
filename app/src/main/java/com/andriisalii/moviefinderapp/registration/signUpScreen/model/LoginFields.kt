package com.andriisalii.moviefinderapp.registration.signUpScreen.model

data class LoginFields(var email: String, var password: String) {
    constructor() : this("", "")
}