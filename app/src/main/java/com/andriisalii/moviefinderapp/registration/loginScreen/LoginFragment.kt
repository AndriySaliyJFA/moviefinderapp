package com.andriisalii.moviefinderapp.registration.loginScreen

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import com.andriisalii.moviefinderapp.MainActivity
import com.andriisalii.moviefinderapp.R
import com.andriisalii.moviefinderapp.registration.signUpScreen.SignUpFragment
import kotlinx.android.synthetic.main.fragment_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class LoginFragment : Fragment() {

    companion object {
        const val KEY_STATE: String = "isLogined"
    }

    enum class LaunchStates {
        LAUNCH_DEFAULT,
        LAUNCH_AS_GUEST,
        LAUNCH_FOR_LOGIN,
        LAUNCH_FOR_LOGINED,
        LAUNCH_FOR_REGISTRATION
    }

    private val mLoginViewModel: LoginViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return View.inflate(context, R.layout.fragment_login, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //region OnClickListeners
        btn_sign_up.setOnClickListener {
            startSignUpFragment(LaunchStates.LAUNCH_FOR_REGISTRATION)
        }

        btn_login_with_email.setOnClickListener {
            startSignUpFragment(LaunchStates.LAUNCH_FOR_LOGIN)
        }

        tv_skip.setOnClickListener {
            startMainActivity(LaunchStates.LAUNCH_AS_GUEST)
        }
        //endregion
        //region ViewModelObservers
        mLoginViewModel.launchState.observe(viewLifecycleOwner) { launchState ->
            handleLaunchState(
                launchState
            )
        }
        //endregion
        mLoginViewModel.checkLaunchState()
    }

    private fun handleLaunchState(state: LaunchStates) {
        when (state) {
            LaunchStates.LAUNCH_DEFAULT, LaunchStates.LAUNCH_FOR_REGISTRATION -> {
                tv_skip.visibility = View.VISIBLE
            }
            LaunchStates.LAUNCH_AS_GUEST -> {
                startMainActivity(LaunchStates.LAUNCH_AS_GUEST)
            }
            LaunchStates.LAUNCH_FOR_LOGIN -> {
                tv_skip.visibility = View.INVISIBLE
            }
            LaunchStates.LAUNCH_FOR_LOGINED -> startMainActivity(
                LaunchStates.LAUNCH_FOR_LOGINED
            )
        }
    }

    private fun startMainActivity(state: LaunchStates) {
        val bundle = Bundle()
        bundle.putInt(KEY_STATE, state.ordinal)
        startActivity(Intent(context, MainActivity::class.java).putExtras(bundle))
        activity!!.finish()
    }

    private fun startSignUpFragment(state: LaunchStates) {
        val bundle = Bundle()
        bundle.putInt(KEY_STATE, state.ordinal)

        val signUpFragment = SignUpFragment()
        signUpFragment.arguments = bundle

        activity!!.supportFragmentManager.beginTransaction()
            .replace(R.id.login_main_container, signUpFragment)
            .addToBackStack(null)
            .commit()
    }
}