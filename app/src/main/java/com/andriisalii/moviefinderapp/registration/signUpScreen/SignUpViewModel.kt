package com.andriisalii.moviefinderapp.registration.signUpScreen

import android.util.Log
import android.view.View.OnFocusChangeListener
import android.widget.EditText
import androidx.databinding.BindingAdapter
import androidx.lifecycle.ViewModel
import com.andriisalii.moviefinderapp.util.SingleLiveEvent
import com.andriisalii.moviefinderapp.registration.loginScreen.LoginFragment
import com.andriisalii.moviefinderapp.registration.repository.UserDataRepository
import com.andriisalii.moviefinderapp.registration.signUpScreen.model.LoginForm


class SignUpViewModel(
    private val userDataRepository: UserDataRepository
) : ViewModel() {

    private val TAG = "SignUpViewModel"

    lateinit var launchState: LoginFragment.LaunchStates
    val isSignInSuccessful = SingleLiveEvent<Boolean>()
    val errorMessage = SingleLiveEvent<String>()
    val loginForm: LoginForm = LoginForm()
    var onEmailFocus: OnFocusChangeListener
    var onPasswordFocus: OnFocusChangeListener

    //region Binding adapters
    object OnFocusBindingAdapter {
        @BindingAdapter("onFocus")
        @JvmStatic
        fun bindFocusChange(
            editText: EditText,
            onFocusChangeListener: OnFocusChangeListener?
        ) {
            if (editText.onFocusChangeListener == null) {
                editText.onFocusChangeListener = onFocusChangeListener
            }
        }
    }
    //endregion

    init {
        onEmailFocus = OnFocusChangeListener { view, focused ->
            val et = view as EditText
            if (focused) {
                Log.d(TAG, "Focus changed to email")
                loginForm.clearEmailError()
            } else {
                Log.d(TAG, "Focus changed from email")
                loginForm.checkEmail(et.text.isNotEmpty())
            }
        }

        onPasswordFocus = OnFocusChangeListener { view, focused ->
            val et = view as EditText
            if (focused) {
                Log.d(TAG, "Focus changed to password")
                loginForm.clearPasswordError()
            } else {
                Log.d(TAG, "Focus changed from password")
                loginForm.checkPassword(et.text.isNotEmpty())
            }
        }
    }

    fun onSignButtonClicked() {
        when (launchState) {
            LoginFragment.LaunchStates.LAUNCH_FOR_LOGIN -> onLoginButtonClicked()
            LoginFragment.LaunchStates.LAUNCH_FOR_REGISTRATION -> onCreateButtonClicked()
            else -> throw RuntimeException("No such state for this screen")
        }
    }

    private fun onCreateButtonClicked() {
        userDataRepository.createUserWithEmailAndPassword(
            loginForm.loginFields.email,
            loginForm.loginFields.password
        ).addOnCompleteListener { task ->
            isSignInSuccessful.value = task.isSuccessful
        }.addOnFailureListener { exception ->
            errorMessage.value = exception.localizedMessage
        }
    }

    private fun onLoginButtonClicked() {
        userDataRepository.signInWithEmailAndPassword(
            loginForm.loginFields.email,
            loginForm.loginFields.password
        ).addOnCompleteListener { task ->
            isSignInSuccessful.value = task.isSuccessful
        }.addOnFailureListener { exception ->
            errorMessage.value = exception.localizedMessage
        }
    }
}