package com.andriisalii.moviefinderapp.registration.repository

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult

class UserDataRepository(var firebaseManager: FirebaseManager) : IUserData {

    override fun isUserGuest(): Boolean {
        return firebaseManager.isUserGuest()
    }

    override fun isUserLoggined(): Boolean {
        return firebaseManager.isUserLoggined()
    }

    override fun signInWithEmailAndPassword(email: String, password: String): Task<AuthResult> {
        return firebaseManager.signInWithEmailAndPassword(email, password)
    }

    override fun createUserWithEmailAndPassword(email: String, password: String): Task<AuthResult> {
        return firebaseManager.createUserWithEmailAndPassword(email, password)
    }
}